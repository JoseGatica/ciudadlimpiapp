import { NgModule } from '@angular/core';
import { IonicPageModule, /*IonicModule*/ } from 'ionic-angular';
import { ReportPage } from './report';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { Camera, StatusBar } from '@angular/core';

@NgModule({
  declarations: [ReportPage],
  imports: [
    IonicPageModule.forChild(ReportPage)],
    providers: [
      //StatusBar,
      SplashScreen,
      //Camera,
      
    ],
    //bootstrap: [AppComponent]
})
export class ReportPageModule {}

