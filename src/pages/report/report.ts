import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
//import { File } from '@ionic-native/file/ngx';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { FormBuilder, /*FormGroup,*/ Validators } from '@angular/forms';

/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
  
})

export class ReportPage {

  authForm: any;
  sendNotification: any;
  hideForm: any;

  constructor(public navCtrl: NavController, private http: Http, public navParams: NavParams, public camera: Camera, public fb: FormBuilder) {
    this.authForm = fb.group({
      'name': [null, Validators.compose([Validators.required])],
      'dni': [null, Validators.compose([Validators.required])],
      'mail': [null, Validators.compose([Validators.required])],
      'phone': [null, Validators.compose([Validators.required])],
      'location': [null, Validators.compose([Validators.required])],
      'recipients': "reporte@ciudadlimpia.cl",
      'subject': "Ciudad Limpia - Reporte de microbasural"
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }

  submitForm(value: any):void{
    var link = "https://ciudadlimpia.cl/formularios/formmail.php";
    var bodyjson = {realname:value.name, recipients:value.recipients, email:value.mail, phone:value.phone, location:value.location, subject:value.subject}
    //var body = JSON.stringify(bodyjson);

    let urlSearchParams = new URLSearchParams();
    for (let key in bodyjson) {
	urlSearchParams.append(key, bodyjson[key]);
    }


    var headers = new Headers();
    headers.append('Content-Type','application/x-www-form-urlencoded');
    alert("DATA: "+urlSearchParams.toString());

    this.http.post(link, urlSearchParams.toString(), {headers: headers})
    .subscribe(data => {
      console.log("DATA: "+ data);
      alert("post: "+ data);
      this.hideForm = true;
      this.sendNotification('Su mensaje ha sido enviado con éxito. GRACIAS');
    },
      err => {
        console.log("ERROR!: ", err);
        alert(err);
      }
    );
  }

  async takePicture(source) {
    try {
      let cameraOptions: CameraOptions = {
        quality: 50,
        targetWidth: 800,
        targetHeight: 1000,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        allowEdit: true,
      };
      cameraOptions.sourceType = (source === 'camera') ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY;
      const result = await this.camera.getPicture(cameraOptions);
      const image = 'data:image/jpeg;base64, ' + result;
      console.log(image);
    } catch(e) {
      console.error(e);
    }
  } 
}

