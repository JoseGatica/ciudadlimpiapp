import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ReportPage } from '../report/report';
import { SlidesPage } from '../slides/slides';
import { AboutPage } from '../about/about';
import { AliadosPage } from '../aliados/aliados';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  goToReport() {
    this.navCtrl.push(ReportPage);
  }
  goToSlides() {
    this.navCtrl.push(SlidesPage);
  }
  goToMap() {
    this.navCtrl.push(AboutPage);
  }
  goToFriends() {
    this.navCtrl.push(AliadosPage);
  }
}
