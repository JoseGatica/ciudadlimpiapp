import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { leaflet, Map, latLng, tileLayer, /*Layer,*/ marker } from 'leaflet';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class AboutPage {

  constructor(public navCtrl: NavController, public geolocation: Geolocation) {  }
  
  map: Map;

  ionViewDidEnter() { this.leafletMap(); }

  leafletMap() {

    var lat= -39.8142204;
    var long= -73.2458878;

    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      lat = (resp.coords.latitude);
      long = (resp.coords.longitude);
      console.log(String(resp.coords.latitude), String(resp.coords.longitude));
      console.log(lat, long);
     }).catch((error) => {
       console.log('Error getting location', error);
     });

    // In setView add latLng and zoom
    //this.map = new Map('mapId').setView([-39.8142204, -73.2458878], 17);
    this.map = new Map('mapId').setView([lat, long], 15);
    tileLayer('http://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'www.ciudadlimpia.cl © Ciudad Limpia',
    }).addTo(this.map);
  
      let watch = this.geolocation.watchPosition();
      watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
      console.log(data.coords.latitude, data.coords.longitude);
      marker([data.coords.latitude, data.coords.longitude]).addTo(this.map)
      .bindPopup('Usted está AQUÍ.')
      .openPopup();
      });

    //marker([-39.8142204, -73.2458878]).addTo(this.map)
    console.log(lat, long);
    //marker([lat, long]).addTo(this.map)
    //  .bindPopup('Usted está AQUÍ.')
    //  .openPopup();
  }

  /** Remove map when we have multiple map object */
  ionViewWillLeave() {
    this.map.remove();
  }

}

