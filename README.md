# CiudadLimpiApp

Our awesome project, from Valdilluvia to the whole world.
<br><br>
<hr>

## Some bugs we had to fix on the road

We had some problems with Angular 6 on Ionic 4. We don't know if it was because an Ionic or an Angular upgrade.
<hr>

* So, I (José Gatica) get a "Can not find module 'ionic-angular'" error, and that made impossible run ionic serve too (and others reasons).

### Solution
* There are some breaking changes in rxjs 6, a workaround for now is to install rxjs-compat: 'npm install --save rxjs-compat' which solves the problem with rxjs.<br>
You can find more possible solutions here: https://github.com/zyra/ionic-image-loader/issues/159

<hr>

* Another error was "Typescript error '=' expected", and was some TypeScript & Node Modules problem. This was the main reason to Ionic Serve don't work.

### Solution
* I updated typescript by typing npm install typescript@2.7.2 and worked.<br>
You can find more possible solutions here: https://forum.ionicframework.com/t/typescript-error-expected-and-more/181140/9

<hr>