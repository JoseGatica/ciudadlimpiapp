webpackJsonp([2],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_leaflet__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_leaflet___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_leaflet__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, geolocation) {
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
    }
    AboutPage.prototype.ionViewDidEnter = function () { this.leafletMap(); };
    AboutPage.prototype.leafletMap = function () {
        var _this = this;
        var lat = -39.8142204;
        var long = -73.2458878;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            lat = (resp.coords.latitude);
            long = (resp.coords.longitude);
            console.log(String(resp.coords.latitude), String(resp.coords.longitude));
            console.log(lat, long);
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
        // In setView add latLng and zoom
        //this.map = new Map('mapId').setView([-39.8142204, -73.2458878], 17);
        this.map = new __WEBPACK_IMPORTED_MODULE_2_leaflet__["Map"]('mapId').setView([lat, long], 15);
        Object(__WEBPACK_IMPORTED_MODULE_2_leaflet__["tileLayer"])('http://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'www.ciudadlimpia.cl © Ciudad Limpia',
        }).addTo(this.map);
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
            // data.coords.longitude
            console.log(data.coords.latitude, data.coords.longitude);
            Object(__WEBPACK_IMPORTED_MODULE_2_leaflet__["marker"])([data.coords.latitude, data.coords.longitude]).addTo(_this.map)
                .bindPopup('Usted está AQUÍ.')
                .openPopup();
        });
        //marker([-39.8142204, -73.2458878]).addTo(this.map)
        console.log(lat, long);
        //marker([lat, long]).addTo(this.map)
        //  .bindPopup('Usted está AQUÍ.')
        //  .openPopup();
    };
    /** Remove map when we have multiple map object */
    AboutPage.prototype.ionViewWillLeave = function () {
        this.map.remove();
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Ruta Camión Recolector\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n\n<!-- Input with placeholder -->\n\n<div id="mapId" style="width: 100%; height: 100%">\n</div>\n\n<!--\n<TABLE>\n<td>\n  <ion-input type="text" placeholder="Ingrese calle o sector"></ion-input>\n</td>\n<td>  \n<button ion-button color="primary">Ver</button>\n</td>\n</TABLE>\n-->\n\n</ion-content>'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlidesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__aliados_aliados__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SlidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SlidesPage = /** @class */ (function () {
    function SlidesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SlidesPage.prototype.goToFriends = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__aliados_aliados__["a" /* AliadosPage */]);
    };
    SlidesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SlidesPage');
    };
    SlidesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-slides',template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/slides/slides.html"*/'<!--\n  Generated template for the SlidesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Conoce CiudadLimpiApp</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding style="background-image: url(\'assets/imgs/valdivia.jpg\');">\n\n    <ion-slides pager="true">\n\n        <ion-slide>\n          <img style="width: 90%; max-width: 350px;" src="assets/imgs/logoCLcolor.png">\n        </ion-slide>\n    \n        <ion-slide>\n          <ion-card>\n            <p style="text-align: justify;">\n              Esta es una app de prueba de <code>Ciudad LimpiApp</code>, ¡y Esperamos estar online muy pronto!\n            </p>\n          </ion-card>\n        </ion-slide>\n      \n        <ion-slide>\n          <ion-card>\n            <p style="text-align: justify;">\n              Puedes estar atento(a) a nuestras Redes Sociales para saber más acerca de <code>Ciudad LimpiApp</code>,\n              y estar informado(a) del lanzamiento y actualizaciones.\n            </p>\n          </ion-card>\n        </ion-slide>\n      \n        <ion-slide>\n          <ion-card>\n            <p style="text-align: center;">Saludos,</p>\n            <p style="text-align: center;">El maravilloso equipo de <code><b>Ciudad LimpiApp</b></code></p>\n          </ion-card>\n        </ion-slide>\n    \n        <ion-slide>\n          <ion-card>\n            <p style="text-align: center;">Te invitamos a conocer</p>\n            <p style="text-align: center;">a algunos de nuestros \n                <button color="secondary" style="width: 80%; height: 3em; border-radius: 10px;" ion-button (click)="goToFriends()"><code><b>amigos y proveedores<br>de servicios</b></code></button></p>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n\n</ion-content>\n'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/slides/slides.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], SlidesPage);
    return SlidesPage;
}());

//# sourceMappingURL=slides.js.map

/***/ }),

/***/ 112:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 112;

/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/aliados/aliados.module": [
		279,
		1
	],
	"../pages/slides/slides.module": [
		280,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 153;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="" tabIcon="home"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { Validators, FormBuilder, FormGroup } from '@angular/forms';
var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contacto\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding style="background-image: url(\'assets/imgs/valdivia.jpg\');">\n  <ion-list>\n    <ion-list-header>Si ves un microbasural, <b>¡denuncia!</b></ion-list-header>\n    <ion-item>\n      <ion-icon name="trash" item-start></ion-icon>\n      <button ion-button color="primary">Reportar Microbasural</button>\n    </ion-item>\n  </ion-list>\n\n  <br>\n</ion-content>\n\n\n<ion-footer>      \n  <ion-item>\n    <p style="text-align: center;">www.ciudadlimpia.cl</p>\n  </ion-item>\n</ion-footer>'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__report_report__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__slides_slides__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__about_about__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__aliados_aliados__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.goToReport = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__report_report__["a" /* ReportPage */]);
    };
    HomePage.prototype.goToSlides = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__slides_slides__["a" /* SlidesPage */]);
    };
    HomePage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__about_about__["a" /* AboutPage */]);
    };
    HomePage.prototype.goToFriends = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__aliados_aliados__["a" /* AliadosPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/home/home.html"*/'\n<ion-header>\n  <ion-navbar>\n    <ion-title color="primary" align="center">Ciudad LimpiApp</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding style="background-image: url(\'assets/imgs/valdivia.jpg\'); background-size: cover;">\n\n<!-- \n  **This is a phone call test button, uncomment it just if you are sure to use it**\n  <a button href="tel:+56986009201" clear>\n    <ion-icon name="call"></ion-icon>\n  </a>\n-->\n<ion-grid>\n  <button color="secondary" style="height: 5em; border-radius: 10px;" ion-button (click)="goToSlides()">Conoce CiudadLimpiApp</button>\n  <button color="danger" style="height: 5em; border-radius: 10px;" ion-button (click)="goToMap()">Seguimiento de Recolector</button>\n  <button color="micro" style="height: 5em; border-radius: 10px;" ion-button (click)="goToReport()">Reportar Microbasural</button>\n  <button color="services" style="height: 5em; border-radius: 10px;" ion-button (click)="goToFriends()">Proyectos amigos</button>\n</ion-grid>\n\n</ion-content>\n<!--\n<ion-footer>\n  <p style="text-align: center; color: orangered;">Ciudad LimpiApp 2019, Valdivia-Chile</p>\n</ion-footer>\n-->'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



//import { File } from '@ionic-native/file/ngx';


/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
//@IonicPage()
var ReportPage = /** @class */ (function () {
    function ReportPage(navCtrl, http, navParams, camera, fb) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        this.camera = camera;
        this.fb = fb;
        this.authForm = fb.group({
            'name': [null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            'dni': [null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            'mail': [null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            'phone': [null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            'location': [null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            'recipients': "reporte@ciudadlimpia.cl",
            'subject': "Ciudad Limpia - Reporte de microbasural"
        });
    }
    ReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReportPage');
    };
    ReportPage.prototype.submitForm = function (value) {
        var _this = this;
        var link = "https://ciudadlimpia.cl/formularios/formmail.php";
        var bodyjson = { realname: value.name, recipients: value.recipients, email: value.mail, phone: value.phone, location: value.location, subject: value.subject };
        //var body = JSON.stringify(bodyjson);
        var urlSearchParams = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* URLSearchParams */]();
        for (var key in bodyjson) {
            urlSearchParams.append(key, bodyjson[key]);
        }
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        alert("DATA: " + urlSearchParams.toString());
        this.http.post(link, urlSearchParams.toString(), { headers: headers })
            .subscribe(function (data) {
            console.log("DATA: " + data);
            alert("post: " + data);
            _this.hideForm = true;
            _this.sendNotification('Su mensaje ha sido enviado con éxito. GRACIAS');
        }, function (err) {
            console.log("ERROR!: ", err);
            alert(err);
        });
    };
    ReportPage.prototype.takePicture = function (source) {
        return __awaiter(this, void 0, void 0, function () {
            var cameraOptions, result, image, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cameraOptions = {
                            quality: 50,
                            targetWidth: 800,
                            targetHeight: 1000,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true,
                            allowEdit: true,
                        };
                        cameraOptions.sourceType = (source === 'camera') ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY;
                        return [4 /*yield*/, this.camera.getPicture(cameraOptions)];
                    case 1:
                        result = _a.sent();
                        image = 'data:image/jpeg;base64, ' + result;
                        console.log(image);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-report',template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/report/report.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Reportar Microbasural</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding style="background-image: url(\'assets/imgs/valdivia.jpg\'); background-size: cover;">\n\n  <ion-card style="border-radius:10px; opacity: 0.5;">\n    <ion-card-content style="text-align: justify;">\n      Si ve un microbasural REPÓRTELO y el departamento de Aseo y Ornato de la Municipalidad lo solucionará en breve\n    </ion-card-content>\n  </ion-card>\n\n  <br>\n\n  <form [formGroup]="authForm" (ngSubmit)="submitForm(authForm.value)" style="background-color: white; border: solid lightyellow 3px; border-radius: 10px; opacity: 0.8;"><br>\n    <ion-item>\n      <ion-label>Nombre:</ion-label>\n      <ion-input type="text" placeholder="John Petrucci" style="color: #39C701;" [formControl]="authForm.controls[\'name\']"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>R.U.N.:</ion-label>\n      <ion-input type="text" placeholder="11111111-1" style="color: #39C701;" [formControl]="authForm.controls[\'dni\']"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>E-mail:</ion-label>\n      <ion-input type="text" placeholder="sucorreo@dominio.com" style="color: #39C701;" [formControl]="authForm.controls[\'mail\']"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Teléfono:</ion-label>\n      <ion-input type="number" placeholder="912345678" style="color: #39C701;" [formControl]="authForm.controls[\'phone\']"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Ubicación:</ion-label>\n      <ion-input type="text" placeholder="Calle, sector, ciudad" style="color: #39C701;" [formControl]="authForm.controls[\'location\']"></ion-input>\n    </ion-item>\n    <ion-item>\n      <div class="form-group">\n        <input type="button" value="Subir Foto" onClick="uploadFile()" class="rm-button" />\n      </div>\n    </ion-item>\n    <!--\n    <ion-item> \n        <button ion-button (click)="takePicture(\'camera\')">\n          <ion-icon name="camera"></ion-icon>\n        </button>\n        <p>Subir foto</p>\n    </ion-item>\n    -->\n    <ion-grid>\n      <!--<button color="medium" size="full" ion-button (click)="logForm()">ENVIAR</button>-->\n      <button mode="md" color="formButton" type="submit" button-md ion-button block tappable>ENVIAR</button>\n    </ion-grid>\n  </form>\n\n</ion-content>\n'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/report/report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */]])
    ], ReportPage);
    return ReportPage;
}());

//# sourceMappingURL=report.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(226);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_about_about__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_report_report__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_slides_slides__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_aliados_aliados__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_in_app_browser__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_camera__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_report_report__["a" /* ReportPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_slides_slides__["a" /* SlidesPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_aliados_aliados__["a" /* AliadosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/aliados/aliados.module#AliadosPageModule', name: 'AliadosPage', segment: 'aliados', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/slides/slides.module#SlidesPageModule', name: 'SlidesPage', segment: 'slides', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_report_report__["a" /* ReportPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_slides_slides__["a" /* SlidesPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_aliados_aliados__["a" /* AliadosPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_camera__["a" /* Camera */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AliadosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AliadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AliadosPage = /** @class */ (function () {
    function AliadosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AliadosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AliadosPage');
    };
    AliadosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-aliados',template:/*ion-inline-start:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/aliados/aliados.html"*/'<!--\n  Generated template for the AliadosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Aliados</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding style="background-color: lightgreen;">\n\n<!--\n    <ion-grid>\n      <button color="light" style="height: 7em; border-radius: 10px;" ion-button (click)="goToSlides()"><img src="http://www.recyvald.cl/wp-content/uploads/2019/05/RecyVald_720px.png"><a href="http://www.recyvald.cl"></a></button>\n      <ion-row>\n          <ion-col size="6">\n              <button color="light" style="height: 7em; border-radius: 10px;" ion-button (click)="goToMap()"><img src="https://scontent.fccp2-1.fna.fbcdn.net/v/t1.0-9/40510747_261235081168414_2875639896419074048_n.jpg?_nc_cat=107&_nc_oc=AQnq-5zIvTto2_N-PP2Cz6N9_j3c6Dxrk4WQId9OI4qVBHWitzKpFStnLNUlHDpOj54&_nc_ht=scontent.fccp2-1.fna&oh=7d2e5a99686d4f1553710d3b20370c0c&oe=5DBAA3A0"></button>\n          </ion-col>\n          <ion-col size="6">\n              <button color="light" style="height: 7em; border-radius: 10px;" ion-button (click)="goToReport()"><img src="https://scontent.fccp2-1.fna.fbcdn.net/v/t1.0-9/53165229_305983296756732_4442238816886980608_n.jpg?_nc_cat=102&_nc_oc=AQl4abXsmMyUr7PvkfoSxhUWiIvJhYw0a1-TtlnQwsfLp65kDQzwW5OMXZP8QLI9QnI&_nc_ht=scontent.fccp2-1.fna&oh=16e9a6ab196fe8a37af3fb209bc4846f&oe=5DB07B1C"></button>\n          </ion-col>\n      </ion-row>\n      <button color="secondary" style="height: 7em; border-radius: 10px;" ion-button (click)="goToFriends()"><img src="https://valdiviasinbasura.cl/wp-content/uploads/2019/04/logo-cooperativa-blanco.png"></button>\n    </ion-grid>\n  -->\n\n    <ion-chip style="width: 100%; height: 5em; border-radius: 30px;">\n      <ion-avatar style="size: 100%; width: 5em;height: 5em">\n	<img src="https://lamanzana.coop/wp-content/uploads/2020/12/logoLaManzana-medio.png">\n      </ion-avatar>\n      <ion-label>Cooperativa La Manzana</ion-label>\n    </ion-chip>\n\n    <ion-chip style="width: 100%; height: 5em; border-radius: 30px;">\n      <ion-avatar style="size: 100%; width: 5em;height: 5em">\n        <img src="http://www.ingenieria.uach.cl/images/innoving/cubosI2030.png">\n      </ion-avatar>\n      <ion-label>Proyecto InnovING 2030</ion-label>\n    </ion-chip>\n\n    <ion-chip style="width: 100%; height: 5em; border-radius: 30px;">\n      <ion-avatar style="size: 100%; width: 5em;height: 5em">\n        <img src="https://www.losrioscb.cl/templates/yootheme/cache/MunicipalidaddeValdivia-logo-b8ef1765.png">\n      </ion-avatar>\n      <ion-label>I. Municipalidad de Valdivia</ion-label>\n    </ion-chip>\n    \n    <!-- \n    <br>\n    <ion-chip style="width: 100%; height: 5em; border-radius: 30px;">\n      <ion-avatar style="size: 100%; width: 5em;height: 5em">\n        <img src="http://www.recyvald.cl/wp-content/uploads/2019/05/RecyVald_720px.png">\n      </ion-avatar>\n      <ion-label>RECYVALD</ion-label>\n    </ion-chip>\n    \n    <br>\n\n    <ion-chip style="width: 100%; height: 5em; border-radius: 30px;">\n      <ion-avatar style="size: 100%; width: 5em;height: 5em">\n        <img src="/assets/imgs/grunKompost.jpg">\n      </ion-avatar>\n      <ion-label>GRÜN KOMPOST</ion-label>\n    </ion-chip>\n    \n    <br>\n\n    <ion-chip style="width: 100%; height: 5em; border-radius: 30px;">\n      <ion-avatar style="size: 100%; width: 5em;height: 5em">\n        <img src="/assets/imgs/circula.jpg">\n      </ion-avatar>\n      <ion-label>CIRCULA</ion-label>\n    </ion-chip>\n    \n    <br>\n\n    <ion-chip style="width: 100%; height: 5em; border-radius: 30px;">\n      <ion-avatar style="size: 100%; width: 5em;height: 5em">\n        <img src="/assets/imgs/valdiviasinbasura.png">\n      </ion-avatar>\n      <ion-label>VALDIVIA SIN BASURA</ion-label>\n    </ion-chip>\n    \n    <br>\n    -->\n</ion-content>\n'/*ion-inline-end:"/home/josegatica/Proyectos/ciudadlimpiapp/src/pages/aliados/aliados.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AliadosPage);
    return AliadosPage;
}());

//# sourceMappingURL=aliados.js.map

/***/ })

},[203]);
//# sourceMappingURL=main.js.map